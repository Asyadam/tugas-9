<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "Nama Domba: " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah kaki: " . $sheep->legs . "<br>"; // 4
echo "Berdarah dingin: " . $sheep->isColdBlooded() . "<br><br>"; // "no"



$kodok = new Frog("Buduk");
echo "Nama Kodok: " . $kodok->name . "<br>";
echo "Jumlah kaki: " . $kodok->legs . "<br>";
echo "Berdarah dingin: " . $kodok->isColdBlooded() . "<br>";
$kodok->jump();
echo"<br> <br>";


$sungkong = new Ape("Kera Sakti");
echo "Nama Kera: " . $sungkong->name . "<br>";
echo "Jumlah kaki: " . $sungkong->legs . "<br>";
echo "Berdarah dingin: " . $sungkong->isColdBlooded() . "<br>"; 
$sungkong->yell();


?>
